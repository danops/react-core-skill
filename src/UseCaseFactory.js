import React from "react";

class Example {
  execute() {
    return "Things and stuff";
  }
}

const UseCaseFactory = React.createContext({
  getUseCase: useCase => {
    const useCases = {
      doExample: new Example()
    };

    return useCases[useCase];
  }
});

export default UseCaseFactory;
