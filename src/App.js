import React, { Component } from "react";
import UseCaseFactory from "./UseCaseFactory";
import "./App.css";

class ExampleFactoryUser extends React.Component {
  render() {
    return (
      <div>{this.props.useCaseFactory.getUseCase("doExample").execute()}</div>
    );
  }
}

const BoundFactoryUser = () => (
  <UseCaseFactory.Consumer>
    {factory => <ExampleFactoryUser useCaseFactory={factory} />}
  </UseCaseFactory.Consumer>
);

class App extends Component {
  render() {
    return <BoundFactoryUser />;
  }
}

export default App;
