import React from "react";

export default class ExpandingFormCabinet extends React.Component {
  constructor() {
    super();

    this.state = { open: false };
  }

  toggleCabinet = () => {
    this.setState({open: !this.state.open})
  }

  renderCabinet = () => {
    return (
      <div style={{ backgroundColor: "hotpink" }}>{this.props.children}</div>
    );
  };

  renderArrow = () => {
    if (this.state.open) {
      return <span>V</span>;
    } else {
      return <span>></span>;
    }
  };

  render() {
    return (
      <div>
        <h1 onClick={this.toggleCabinet}>
          Click me to expand! {this.renderArrow()}
        </h1>
        {this.state.open && this.renderCabinet()}
      </div>
    );
  }
}
