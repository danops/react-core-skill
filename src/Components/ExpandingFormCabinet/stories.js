import React from "react";
import { storiesOf } from "@storybook/react";
import ExpandingFormCabinet from ".";

storiesOf("ExpandingFormCabinet", module)
  .add("Default", () => (
    <ExpandingFormCabinet>
      <h1>I'm inside the cabinet!</h1>
    </ExpandingFormCabinet>
  ))
  .add("Another", () => (
    <ExpandingFormCabinet>
      <ExpandingFormCabinet>
        <h1>too deep for me</h1>
      </ExpandingFormCabinet>
    </ExpandingFormCabinet>
  ));
