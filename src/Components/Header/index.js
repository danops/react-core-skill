import React from "react";
import './style.css'

export default class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <h1 className="logo">Header</h1>
        <span className="nav-item">Home</span>
        <span className="nav-item">Dogs</span>
      </div>
    );
  }
}
