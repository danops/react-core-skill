import React from "react";
import List from ".";
import { storiesOf } from "@storybook/react";

storiesOf("List", module)
  .add("Single", () => <List title="A simple list" items={["First"]} />)
  .add("Double", () => <List title="A less simple list" items={["First", "Second"]} />);
