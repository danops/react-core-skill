import React from "react";

export default class List extends React.Component {
  renderItems = () =>
    this.props.items.map((item, i) => <div key={i}>{item}</div>);

  render() {
    return (
      <div>
        <h1>{this.props.title}</h1>
        {this.renderItems()}
      </div>
    );
  }
}
