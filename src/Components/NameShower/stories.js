import React from "react";
import { storiesOf } from "@storybook/react";
import LoggingInput from ".";
import NameShower from ".";

storiesOf("NameShower", module)
  .add("A", () => <NameShower getName={() => "Dan"} />)
  .add("B", () => <NameShower getName={() => "Dave"} />);
