import React from "react";

export default class NameShower extends React.Component {
  render() {
    return (
      <div>
        <h1>{this.props.getName()}</h1>
      </div>
    );
  }
}
