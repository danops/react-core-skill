import React from "react";
import { storiesOf } from "@storybook/react";
import LoggingInput from ".";

storiesOf("LoggingInput", module).add("Default", () => (
  <LoggingInput onChange={value => console.log(value)} />
));
