import React from "react";

export default class Square extends React.Component {
  renderBlack = () => (
    <div
      style={{ width: "100px", height: "100px", backgroundColor: "black" }}
    />
  );
  renderPink = () => (
    <div
      style={{ width: "100px", height: "100px", backgroundColor: "hotpink" }}
    />
  );
  render() {
    return (
      <div>
        {!this.props.pink && this.renderBlack()}
        {this.props.pink && this.renderPink()}
      </div>
    );
  }
}
