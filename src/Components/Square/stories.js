import React from "react";
import Square from ".";
import { storiesOf } from "@storybook/react";

storiesOf("Square", module)
  .add("Flase", () => <Square pink={false} />)
  .add("True", () => <Square pink={true} />);
